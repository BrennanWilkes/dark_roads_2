
/**
	Canvas object
	@type {object}
	@global
	@constant
*/
const canvas = document.getElementById("world");

/**
	2d context of the canvas
	@type {object}
	@global 
	@constant
*/
const ctx = canvas.getContext("2d");

/** 
	ID of mainloop interval 	
	@type {number}
	@global
*/
let interval_id;

//Screen size constants

/** 
	Maximum x coordinate of the canvas 
	@type {number}
	@global 
	@constant
*/
const MAX_X = canvas.width;

/** 
	Maximum y coordinate of the canvas 
	@type {number}
	@global 
	@constant
*/
const MAX_Y = canvas.height;

/** 
	Minimum x coordinate of the canvas 
	@type {number}
	@global 
	@constant
*/
const MIN_X = 0;

/** 
	Minimum y coordinate of the canvas 
	@type {number}
	@global 
	@constant
*/
const MIN_Y = 0;

const PIXEL_SIZE = 20;

const shiftindex = {x:[-1,0,1,0],y:[0,-1,0,1]};

const player = new Player();

let current_page;

function setUp(){

	ctx.font="bolder 20px Arial";
	
	current_page = new Page(MAX_X/PIXEL_SIZE,MAX_Y/PIXEL_SIZE,0,0,"rgb(0, 0, 75)");
	current_page.map[0][0] = new Pixel('^',"gold");
	current_page.map[current_page.xsize-1][current_page.ysize-1] = new Pixel('^',"white");
	
	addEventListener("keyup",keyUp);
	interval_id = setInterval(tick,100);
}



function Pixel(char,colour,x,y){
	this.char = char;
	this.colour = colour;
	this.x = x;
	this.y = y;
}

function Page(xsize,ysize,x,y,bkd){
	this.draw = function(){
		
		ctx.fillStyle = this.bkd;
		ctx.fillRect(0,0,MAX_X,MAX_Y);
		
		for(let x=0;x<this.xsize;x++){
			for(let y=0;y<this.ysize;y++){
				if(this.map[x][y].char!=' ' && (x!=player.x || y!=player.y)){
					ctx.fillStyle = this.map[x][y].colour;
					ctx.fillText(this.map[x][y].char,x*PIXEL_SIZE,(y+1)*PIXEL_SIZE);
				}
			}
		}
	}
	
	this.fill = function(char,colour){
		for(let x=0;x<this.xsize;x++){
			for(let y=0;y<this.ysize;y++){
				this.map[x][y] = new Pixel(char,colour,x,y);
			}
		}
	}
	
	this.xsize = xsize;
	this.ysize = ysize;
	this.x = x;
	this.y = y;
	this.bkd = bkd;
	
	this.map = new Array(xsize);
	for(let i=0;i<xsize;i++){
		this.map[i] = new Array(ysize);
	}
	this.fill(' ');
}

function Player(){
	this.move = function(dir){
		this.dir = dir;
		if(inBounds(this.x+shiftindex.x[dir],this.y+shiftindex.y[dir])){
			this.x += shiftindex.x[dir];
			this.y += shiftindex.y[dir];
		} 
	}
	
	this.draw = function(){
		ctx.fillStyle = "white";
		ctx.fillText('@',this.x*PIXEL_SIZE,(this.y+1)*PIXEL_SIZE);
	}
	
	this.x = 0;
	this.y = 0;
	this.pagex = 0;
	this.pagey = 0;
	this.dir = 0;
	this.inv = new Array();
}

function item(char){
	this.char = char;
}

function tick(){
	current_page.draw();
	player.draw();
}

function inBounds(x,y){
	return (x>=0 && x<current_page.xsize && y>=0 && y<current_page.ysize);
}


function keyUp(event){
	if(event.key.substring(0,5) === "Arrow"){
		player.move(event.keyCode-37);
	}
}

























